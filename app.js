const process = require('child_process')
const fkill = require('fkill')
const remote = require('electron').remote
const DiffCamEngine = require('diff-cam-engine')

const JIGGLE_EXE = __dirname + '/bin/MouseJigglePlus.exe -j -z -m -t'

const video = document.getElementById('video')
const canvas = document.getElementById('motion')
const score = document.getElementById('score')

let jiggleProcess = null

DiffCamEngine.init({
	video: video,
	motionCanvas: canvas,
	initSuccessCallback: () => { DiffCamEngine.start() },
	initErrorCallback: () => { console.error('Error initializing') },
	captureCallback: onCapture,
	captureIntervalTime: 100,
	scoreThreshold: 40,
	pixelDiffThreshold: 32,
	diffWidth: 64,
	diffHeight: 48,
	captureWidth: 320,
	captureHeight: 240,
})

function onCapture(payload) {
	score.textContent = payload.score
	if (payload.hasMotion) {
		console.log("motion detected")
		triggerMouse()
	}
}

function triggerMouse() {
	if (jiggleProcess !== null) return

	console.log('jiggle started')
	jiggleProcess = process.exec(JIGGLE_EXE)
	jiggleProcess.on('exit', () => {
		console.log('jiggle ended')
		jiggleProcess = null
	})
}
